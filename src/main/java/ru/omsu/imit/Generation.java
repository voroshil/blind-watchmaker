package ru.omsu.imit;

import java.io.*;
import java.util.Random;

public class Generation {
    static Random random = new Random();
    public static int populationSize;
    private int generationNumber;

    ClockState[] clocks;

    private Generation() {
        clocks = new ClockState[populationSize];
    }

    public static Generation generateNew(IWatchmaker watchmaker) {
        Generation g = new Generation();
        for(int i=0; i<populationSize;i++){
            Clock clock = new Clock();
            ClockState output = watchmaker.clocktest(clock);
            g.clocks[i] = output;
        }
        return g;
    }

    public static Generation loadFromFile(String filename) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(filename);
        ObjectInputStream ois = new ObjectInputStream(fis);

        int generationNumber = ois.readInt();
        int populationSize = ois.readInt();
        System.out.println(generationNumber);
        System.out.println(populationSize);
        Generation.populationSize = populationSize;

        Generation g = new Generation();
        g.generationNumber = generationNumber;
        for(int i=0;i<populationSize; i++){
            g.clocks[i] = new ClockState();
            g.clocks[i].output1 = null;
            if (generationNumber % 10 == 0){
                g.clocks[i].output1 = new Clock();
                for(int i1=0;i1<40; i1++){
                    for(int j1=0;j1<40;j1++){
                        g.clocks[i].output1.conn[i1][j1] = ois.read();
                    }
                    g.clocks[i].output1.sizes[i1] = ois.readInt();
                }
            }
            g.clocks[i].output2 = ois.readDouble();
            g.clocks[i].output3 = ois.readInt();

            int length = ois.readInt();
            if (length > 0) {
                g.clocks[i].output4_0 = new int[length];
                g.clocks[i].output4_1 = new int[length];
                g.clocks[i].output4_2 = new double[length];
                for (int j = 0; j < length; j++) {
                    g.clocks[i].output4_0[j] = ois.readInt();
                    g.clocks[i].output4_1[j] = ois.readInt();
                    g.clocks[i].output4_2[j] = ois.readDouble();
                }
            }
            length = ois.readInt();
            if (length > 0) {
                g.clocks[i].output5_0 = new int[length];
                g.clocks[i].output5_1 = new int[length];
                g.clocks[i].output5_2 = new double[length];
                for (int j = 0; j < length; j++) {
                    g.clocks[i].output5_0[j] = ois.readInt();
                    g.clocks[i].output5_1[j] = ois.readInt();
                    g.clocks[i].output5_2[j] = ois.readDouble();
                }
            }
        }
        ois.close();
        fis.close();
        return g;
    }

    public static void saveToFile(Generation gen, String filename) throws IOException {
        String fullFilename = String.format(filename, (gen.generationNumber % 100));

        FileOutputStream fos = new FileOutputStream(fullFilename);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeInt(gen.generationNumber);
        oos.writeInt(gen.clocks.length);
        for(int i=0;i<gen.clocks.length; i++){
            ClockState state = gen.clocks[i];
            if (gen.generationNumber % 10 == 0) {
                for(int i1=0;i1<40; i1++){
                    for(int j1=0;j1<40;j1++){
                        oos.write(state.output1.conn[i1][j1]);
                    }
                    oos.writeInt(state.output1.sizes[i1]);
                }
            }
            oos.writeDouble(state.output2);
            oos.writeInt(state.output3);
            if (state.output4_0 != null) {
                oos.writeInt(state.output4_0.length);
                for (int j = 0; j < state.output4_0.length; j++) {
                    oos.writeInt(state.output4_0[j]);
                    oos.writeInt(state.output4_1[j]);
                    oos.writeDouble(state.output4_2[j]);
                }
            }else
                oos.writeInt(0);
            if (state.output5_0 != null) {
                oos.writeInt(state.output5_0.length);
                for (int j = 0; j < state.output5_0.length; j++) {
                    oos.writeInt(state.output5_0[j]);
                    oos.writeInt(state.output5_1[j]);
                    oos.writeDouble(state.output5_2[j]);
                }
            }else
                oos.writeInt(0);
        }
        oos.close();
        fos.close();
    }

    public void doBattle(IWatchmaker watchmaker) {
        int[] ranks = chooseCandidates();

        Clock mother = clocks[ranks[0]].output1;
        Clock father = clocks[ranks[1]].output1;

        Clock offspring = new Clock(mother, father);
        offspring.mutate(0.01, 0.01);

        clocks[ranks[2]] = watchmaker.clocktest(offspring);
    }

    /**
     * @return
     * return[0] - mother
     * return[1] - father
     * return[2] - dead -> offspring
     */
    int[] chooseCandidates(){
        int[] ranks = new int[3];
        ranks[0] = random.nextInt(populationSize);
        ranks[1] = random.nextInt(populationSize);
        ranks[2] = random.nextInt(populationSize);

        while (ranks[0] == ranks[1] || ranks[1]== ranks[2] || ranks[2] ==ranks[0]) {
            ranks[0] = random.nextInt(populationSize);
            ranks[1] = random.nextInt(populationSize);
            ranks[2] = random.nextInt(populationSize);
        }
        if (clocks[ranks[0]].output2 <  clocks[ranks[1]].output2){
            int tmp = ranks[0];
            ranks[0] = ranks[1];
            ranks[1] = tmp;
        }
        if (clocks[ranks[1]].output2 <  clocks[ranks[2]].output2){
            int tmp = ranks[1];
            ranks[1] = ranks[2];
            ranks[2] = tmp;
        }
        return ranks;
    }

    public int getGenerationNumber() {
        return generationNumber;
    }

    public int[] nextGeneration(IWatchmaker watchmaker) {
        generationNumber++;
        for(int N=0; N<Generation.populationSize; N++){
            doBattle(watchmaker);
        }
        int[] types = new int[10];
        for (int j = 0; j < 10; j++) types[j] = 0;
        for (int c = 0; c < Generation.populationSize; c++) {
            if (clocks[c].output3 < 10) {
                types[clocks[c].output3]++;
            }
        }
        return types;
    }
}
