package ru.omsu.imit;

import java.util.Stack;

/**
 * @author voroshil
 * Date: 13.10.2018
 * Time: 20:35
 */
public class OriginalWatchmaker implements IWatchmaker {

	public double[] calcRotations(Clock clock, int[][] gconn, int[]gearsize, int gr, double pendulumPeriod) {
		double[] rotation = new double[30];
		int[] seen = new int[30];
		for(int i=0; i<30; i++) rotation[i] = 0.0;
		double period = pendulumPeriod * gearsize[gr];
		int badgears = 0;
		period = Math.round(period * 1000) / 1000;
		rotation[gr] = period;
		seen[gr]=1;
		int pathlength = 1;
		Stack<Integer> s = new Stack<Integer>();
		s.push(gr);
		while (pathlength > 0) {
			if (pathlength > 40){
				clock.dump();
				for(int i=0;i<s.size();i++){
					System.out.println("s["+i+"]="+s.get(i));
				}
				throw new RuntimeException("Stack overflow");
			}
			int nn = s.peek();
			int count = 0;
			for(int i=0; i<30;i++){
				if (gconn[nn][i] != Clock.NO_CONNECTION)
					count++;
			}

			int[] post = new int[count];
			count=0;
			for(int i=0; i<30;i++){
				if (gconn[nn][i] != Clock.NO_CONNECTION)
					post[count++]=i;
			}

			badgears = 0;
			int foundtip = 0;
			int test;
			for (test = 0; test< post.length; test++){
				double temprot;
				if (gconn[nn][post[test]] == Clock.AXIL_CONNECTION) {
					temprot = rotation[nn];
				} else {
					temprot = -(rotation[nn] * gearsize[post[test]]) / gearsize[nn];
				}

				temprot = (Math.round(temprot * 1000)) / 1000;

				if (seen[post[test]] == 0) {
					seen[post[test]] = 1;
					rotation[post[test]] = temprot;
					s.push(post[test]);
					pathlength = pathlength + 1;
					foundtip = 1;
//                    break;
				} else if(Math.abs(rotation[post[test]] - temprot) > 0.002) {
					badgears = 1;
					break;
				}
			}

			if (badgears == 1) {
				//       %disp('gears');
				break;
			}

			if (test==0 && foundtip == 0) {
				s.pop();
				pathlength = pathlength - 1;
			} else if(test == post.length && foundtip == 0) {
				s.pop();
				pathlength = pathlength - 1;
			}
		}

		//   There are other ways gears can bind.  First check if a hand
		//   connects to two different gears.  At least one must be spinning.
		for(int r=Clock.MIN_HAND; r<=Clock.MAX_HAND; r++) {
			int[] o = clock.findConnected(r, 40);
			if (o.length > 1) {
				int[] g = clock.findConnected(r, 30);
				for (int temp = 0; temp < g.length ; temp++){
					if (rotation[g[temp]] != 0){
						badgears = 1;
						//    %disp('hands');
					}
				}
			}
		}
		if (badgears == 1){
			return new double[0];
		}else
			return rotation;
	}

	private int[] findPendulumWithRatchet(Clock clock, int[] pendulums) {
		int[] grgs = new int[3];
		grgs[0] = -1; // gear with ratchet
		grgs[1] = -1; // gear with spring
		grgs[2] = -1; // pendulum with ratchet

		for (int p = 0; p<pendulums.length; p++){
			if (clock.conn[Clock.RATCHET][pendulums[p]] == Clock.TEETH_CONNECTION) {
				// The ratchet connects to the pendulum.
				// This is the gear the pendulum ratchet connects to
				int[] grs = clock.findByConnectionType(Clock.RATCHET,Clock.AXIL_CONNECTION, 30);
				if (grs.length > 0) {
					grgs[0] = grs[0];
				}

				if (grgs[0] >= 0 && clock.conn[Clock.BASE][grgs[0]] == Clock.NO_CONNECTION) {
					// The gear does not connect to the base.
					grgs[0] = -1;
				}

				if (grgs[0] >= 0) {
					// This is the gear the spring connects to
					int[] gss = clock.findConnected(Clock.SPRING, 30);
					if (gss.length > 0) {
						grgs[1] = gss[0];
					}

					if (grgs[1] >=0 && clock.conn[Clock.BASE][grgs[1]] == Clock.NO_CONNECTION) {
						grgs[1] = -1;
					}
					grgs[2] = p;
				}
			}
		}
		return grgs;
	}

	public int[] findPendulum(Clock clock){
		int p_count = 0;
		int[] pend = new int[clock.MAX_HAND-clock.MIN_HAND+1];
		for (int h = clock.MIN_HAND; h<=clock.MAX_HAND; h++){
			if (clock.conn[clock.BASE][h] != 0){
				int[] g = clock.findConnected(h, 30);
				if (g.length != 1){
					continue;
				}
				int[] tmp = clock.findConnected(g[0], 40);
				if (tmp.length <=1){
					if (clock.sizes[h] > 10000) {
						pend[p_count++] = h;
					}
				}
			}
		}
		if (p_count == 0)
			return new int[0];
		int[] result = new int[p_count];
		for(int i=0; i<p_count; i++){
			result[i] = pend[i];
		}
		return result;
	}

	public static double calcPrecision(double[] pend, int p){
		if (pend.length == 0)
			return 0;

		double result = Math.abs(p - Math.abs(pend[0]));
		for(int i=0; i<pend.length; i++){
			double tmp = Math.abs(p - Math.abs(pend[i]));
			if (tmp < result)
				result = tmp;
		}
		return result / p;
	}

	public static double[] calcScore(double[] pend3){
		//   Test for the pendulum(s) ability to tell various intervals of time.
		double secpend = calcPrecision(pend3,1);
		double minpend = calcPrecision(pend3,60);
		double hrpend = calcPrecision(pend3, 3600);
		double daypend = calcPrecision(pend3, 86400);
		double weekpend = calcPrecision(pend3, 604800);
		double yearpend = calcPrecision(pend3, 31536000);

		double minp = pend3[0];
		for(int i=0; i<pend3.length; i++){
			if (pend3[i] < minp)
				minp = pend3[i];
		}
		double[] score = new double[6];

		//   Prevent scores of infinity.
		if (minp > 31536000) {
			score[0] = 0;
			score[1] = 0;
			score[2] = 0;
			score[3] = 0;
			score[4] = 0;
			score[5] = 0;
		}else if(minp > 604800) {
			score[0] = 0;
			score[1] = 0;
			score[2] = 0;
			score[3] = 0;
			score[4] = 0;
			score[5] = yearpend >= 1e-6 ? 1.0/yearpend : 1e6;
		}else if(minp > 86400) {
			score[0] = 0;
			score[1] = 0;
			score[2] = 0;
			score[3] = 0;
			score[4] = weekpend >= 1e-6 ? 1.0/weekpend : 1e6;
			score[5] = yearpend >= 1e-6 ? 1.0/yearpend : 1e6;
		}else if (minp > 3600) {
			score[0] = 0;
			score[1] = 0;
			score[2] = 0;
			score[3] = daypend >= 1e-6 ? 1.0/daypend : 1e6;
			score[4] = weekpend >= 1e-6 ? 1.0/weekpend : 1e6;
			score[5] = yearpend >= 1e-6 ? 1.0/yearpend : 1e6;
		}else if (minp > 60) {
			score[0] = 0;
			score[1] = 0;
			score[2] = hrpend >= 1e-6 ? 1.0/hrpend : 1e6;
			score[3] = daypend >= 1e-6 ? 1.0/daypend : 1e6;
			score[4] = weekpend >= 1e-6 ? 1.0/weekpend : 1e6;
			score[5] = yearpend >= 1e-6 ? 1.0/yearpend : 1e6;
		}else if (minp > 1) {
			score[0] = 0;
			score[1] = minpend >= 1e-6 ? 1.0/minpend : 1e6;
			score[2] = hrpend >= 1e-6 ? 1.0/hrpend : 1e6;
			score[3] = daypend >= 1e-6 ? 1.0/daypend : 1e6;
			score[4] = weekpend >= 1e-6 ? 1.0/weekpend : 1e6;
			score[5] = yearpend >= 1e-6 ? 1.0/yearpend : 1e6;
		}else{
			score[0] = secpend >= 1e-6 ? 1.0/secpend : 1e6;
			score[1] = minpend >= 1e-6 ? 1.0/minpend : 1e6;
			score[2] = hrpend >= 1e-6 ? 1.0/hrpend : 1e6;
			score[3] = daypend >= 1e-6 ? 1.0/daypend : 1e6;
			score[4] = weekpend >= 1e-6 ? 1.0/weekpend : 1e6;
			score[5] = yearpend >= 1e-6 ? 1.0/yearpend : 1e6;
		}
		return score;
	}

	public ClockState clocktest(Clock clock) {
		ClockState output = new ClockState();
		Clock conn = Clock.cloneTested(clock);
		int gearsize[] = clock.gearSize();
		Clock temp99 = Clock.cloneUntested(clock);
		Clock.copyConnection(temp99, conn);
		output.output1 = temp99;

		int[][] gconn = conn.checkBase();

		//   Check for a pendulum: a hand that is attached to the base
		//   that hand may be attached to a gear, but that gear cannot
		//   be attached to anything else. In this simple simulation a pendulum is
		//   the only form that can create regular motion, this is a simple fact.
		//   If we don't find one there is no need to go on as the clock will not
		//   work no matter how the remaining components are connected.
		int[] pend = findPendulum(conn);
		if (pend.length == 0) {
			output.output2 = 0;
			output.output3 = ClockState.NO_PENDULUM;
			return output;
		}

		output.output3 = ClockState.ONLY_PENDULUM;
		output.output4_0 = pend;
		output.output4_1 = new int[pend.length];
		output.output4_2 = new double[pend.length];
		for(int i=0; i<pend.length; i++){
			int l = conn.sizes[pend[i]] / 10000;
			output.output4_1[i] = l;
			output.output4_2[i] = 2.007 * Math.pow(l, 0.5);
		}
		double[] score = calcScore(output.output4_2);
		output.output2 = score[0] + score[1] + score[2] + score[3] + score[4] +score[5];

		//   Let's search foward from the pendulum.  The only way a pendulum can
		//   transfer motion to gears is through a ratchet.  We are not constraining
		//   who connects where but if things don't line up in a functional way the
		//   clock won't work and there is no need continuing the simulation.  We
		//   onlt simulate as far as we need to go. This saves computer time and
		//   makes the code more compact.
		int[] grgs = findPendulumWithRatchet(clock, output.output4_0);
		int pendulum = grgs[2];
		if (grgs[0]<0 || grgs[1] <0 ) {
			return output;
		}
		output.output3 = ClockState.PENDULUM_AND_RATCHET;


		int[][] d2 = Clock.distanceTo(gconn, grgs);
		if (d2[grgs[0]][grgs[1]] < 0 && grgs[0] != grgs[1]) {
			// The spring gear not connects to the ratchet gear
			return output;
		}
		output.output3 = ClockState.PENDULUM_AND_RATCHET_AND_SPRING;

		//   If you made it here you potentially have a powered clock
		//   must check if the gears turn or if they bind up.
		//   Start with the ratcheted gear and work foward to all gears it
		//   is connected to.  Initial turn rate is 0, the values are updated
		//   as you work through the connections.  If a value to be assigned to
		//   a gear conflicts with a value already there (except 0) that means
		//   the system will not turn.
		double[] rotation = calcRotations(clock, gconn, gearsize, grgs[0], output.output4_2[pendulum]);

		//%   The spring was previously determined to connect to one gear and
		//%   the housing, therefore the spring cannot bind up the gears.
		//%   The ratchet was also previously tested.

		if (rotation.length == 0) {
			return output;
		}

		output.output3 = ClockState.ROTATING_CLOCK;

		//%   If you make it here then the gears do not bind.  Find the hands
		//%   are attached to the gears that move and calculate their period
		//%   moving hands beat out pendulums.  Multiply their scores by 1000.

		int spinrateCount = 0;
		for(int i=0; i<rotation.length; i++){
			if (rotation[i] != 0)
				spinrateCount++;
		}

		double[] spinrate = new double[spinrateCount];
		int[] spinners = new int[spinrateCount];
		spinrateCount=0;
		for(int i=0; i<rotation.length; i++){
			if (rotation[i] != 0) {
				spinrate[spinrateCount] = rotation[i];
				spinners[spinrateCount] = i;
				spinrateCount++;
			}
		}

		//   Here we test the gears ability to measure various intervals of time.
		//   Gears usually beat pendulums since they can spin at much slower rates.
		double spinScore[] = calcScore(spinrate);
		if (score[0] < spinScore[0]) score[0] = spinScore[0];
		if (score[1] < spinScore[1]) score[1] = spinScore[1];
		if (score[2] < spinScore[2]) score[2] = spinScore[2];
		if (score[3] < spinScore[3]) score[3] = spinScore[3];
		if (score[4] < spinScore[4]) score[4] = spinScore[4];
		if (score[5] < spinScore[5]) score[5] = spinScore[5];

		//   Gears cannot keep time below the period of the pendulum.  This feature
		//   might have been added after I made the video.
		if (output.output4_2[pendulum] > 31536000) {
			score[0] = 0.0;
			score[1] = 0.0;
			score[2] = 0.0;
			score[3] = 0.0;
			score[4] = 0.0;
			score[5] = 0.0;
		}else if (output.output4_2[pendulum] > 604800) {
			score[0] = 0.0;
			score[1] = 0.0;
			score[2] = 0.0;
			score[3] = 0.0;
			score[4] = 0.0;
		}else if (output.output4_2[pendulum] > 86400) {
			score[0] = 0.0;
			score[1] = 0.0;
			score[2] = 0.0;
			score[3] = 0.0;
		}else if (output.output4_2[pendulum] > 3600) {
			score[0] = 0.0;
			score[1] = 0.0;
			score[2] = 0.0;
		}else if (output.output4_2[pendulum] > 60) {
			score[0] = 0.0;
			score[1] = 0.0;
		}else if (output.output4_2[pendulum] > 1) {
			score[0] = 0.0;
		}

		output.output2 = score[0] + score[1] + score[2] + score[3] + score[4] +score[5];

		int[] hands = new int[Clock.MAX_HAND-Clock.MIN_HAND+1];
		int[] handsgears = new int[Clock.MAX_HAND-Clock.MIN_HAND+1];
		double[] handsrates = new double[Clock.MAX_HAND-Clock.MIN_HAND+1];
		int handcount = 0;

		//%   Look for hands connected to the gears.
		for (int h=Clock.MIN_HAND; h<=Clock.MAX_HAND; h++){
			int connectionFound = -1;
			for(int i=0; i<spinners.length; i++){
				if (conn.conn[h][spinners[i]] != Clock.NO_CONNECTION){
					connectionFound = i;
					break;
				}
			}
			if (connectionFound >= 0) {
				if (conn.conn[Clock.BASE][h] == Clock.NO_CONNECTION) {
					hands[handcount] = h;
					handsgears[handcount] = spinners[connectionFound];
					handsrates[handcount] = rotation[spinners[connectionFound]];
					handcount = handcount + 1;

				}
			}
		}

		if (handcount == 0) {
			return output;
		}
		if (handcount < hands.length){
			int[] thands = new int[handcount];
			int[] thandsgears = new int[handcount];
			double[] thandsrates = new double[handcount];
			for(int i=0; i<handcount; i++){
				thands[i] = hands[i];
				thandsgears[i] = handsgears[i];
				thandsrates[i] = handsrates[i];
			}
			hands = thands;
			handsgears = thandsgears;
			handsrates = thandsrates;
		}
		output.output3 = ClockState.ROTATING_CLOCK + 1;
		output.output5_0 = hands;
		output.output5_1 = handsgears;
		output.output5_2 = handsrates;

		//%   Test the hands ability to measure various periods of time.
		double sechand = calcPrecision(handsrates, 1);
		double minhand = calcPrecision(handsrates, 60);
		double hrhand = calcPrecision(handsrates, 3600);
		double dayhand = calcPrecision(handsrates, 86400);
		double weekhand = calcPrecision(handsrates, 604800);
		double yearhand = calcPrecision(handsrates, 31536000);
		int[] handuse = new int[6];
		int uniquehand = 0;
		handuse[0] = -1;
		handuse[1] = -1;
		handuse[3] = -1;
		handuse[4] = -1;
		handuse[5] = -1;
		for(int i=0; i<handsrates.length; i++) {
			int idx = -1;
			if(Math.abs(1 - Math.abs(handsrates[i]))/1 == sechand)
				idx = hands[i];
			if(Math.abs(60 - Math.abs(handsrates[i]))/60 == minhand)
				idx = hands[i];
			if(Math.abs(3600 - Math.abs(handsrates[i]))/3600 == hrhand)
				idx = hands[i];
			if(Math.abs(86400 - Math.abs(handsrates[i]))/86400 == dayhand)
				idx = hands[i];
			if(Math.abs(604800 - Math.abs(handsrates[i]))/604800 == weekhand)
				idx = hands[i];
			if(Math.abs(31536000 - Math.abs(handsrates[i]))/31536000 == yearhand)
				idx = hands[i];
			if (idx >=0){
				int j=0;
				for(j=0; j<6; j++){
					if (handuse[j] < 0 || handuse[j] == idx)
						break;
				}
				if (j != 6 && handuse[j] == -1) {
					handuse[j] = idx;
					uniquehand++;
				}
			}
		}

		if (output.output4_2[pendulum] > 31536000) {
			score[0] = 0.0;
			score[1] = 0.0;
			score[2] = 0.0;
			score[3] = 0.0;
			score[4] = 0.0;
			score[5] = 0.0;
		}else if (output.output4_2[pendulum] > 604800) {
			score[0] = 0.0;
			score[1] = 0.0;
			score[2] = 0.0;
			score[3] = 0.0;
			score[4] = 0.0;
			score[5] = Math.abs(1/yearhand);
		}else if (output.output4_2[pendulum] > 86400) {
			score[0] = 0.0;
			score[1] = 0.0;
			score[2] = 0.0;
			score[3] = 0.0;
			score[4]= Math.abs(1/weekhand);
			score[5] = Math.abs(1/yearhand);
		}else if (output.output4_2[pendulum] > 3600) {
			score[0] = 0.0;
			score[1] = 0.0;
			score[2] = 0.0;
			score[3] = Math.abs(1/dayhand);
			score[4] = Math.abs(1/weekhand);
			score[5] = Math.abs(1/yearhand);
		}else if (output.output4_2[pendulum] > 60) {
			score[0] = 0.0;
			score[1] = 0.0;
			score[2] = Math.abs(1/hrhand);
			score[3] = Math.abs(1/dayhand);
			score[4] = Math.abs(1/weekhand);
			score[5] = Math.abs(1/yearhand);
		}else if (output.output4_2[pendulum] > 1) {
			score[0] = 0.0;
			score[1] = Math.abs(1/minhand);
			score[2] = Math.abs(1/hrhand);
			score[3] = Math.abs(1/dayhand);
			score[4] = Math.abs(1/weekhand);
			score[5] = Math.abs(1/yearhand);
		}else{
			score[0] = Math.abs(1/sechand);
			score[1] = Math.abs(1/minhand);
			score[2] = Math.abs(1/hrhand);
			score[3] = Math.abs(1/dayhand);
			score[4] = Math.abs(1/weekhand);
			score[5] = Math.abs(1/yearhand);

		}
		for(int i=0; i<6; i++){
			if (score[i] > 1e6)
				score[i] = 1e6;
		}
		//%   As mentioned in the video, hands on gears are much better than gears
		//%   alone since they allow you to keep track of the exact position of the
		//%   gear.  This way you can look away from the clock and not loose the
		//%   time.  Hands therefore make the clock much better at telling time.
		//%   QUestion is how much better.  Here I multiply the score by 1 million
		//%   since I think hands improve them that much (clocks that you have to
		//%   stare at all the time are pretty crappy), but this value is subjective.
		//%   Play with it and see what happens.
		output.output2 = (score[0] + score[1] + score[2] + score[3] + score[4] + score[5]) * 1e6;

		output.output3 = ClockState.ROTATING_CLOCK + uniquehand;

		//output{6} = gconn;
		return output;
	}

}
