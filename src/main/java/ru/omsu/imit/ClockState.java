package ru.omsu.imit;

public class ClockState {
    // Clock arrows
    public int[] output5_0;
    // Spinning gears
    public int[] output5_1;
    // Spin rate
    public double[] output5_2;

    public static final int NO_PENDULUM = 0;
    public static final int ONLY_PENDULUM = 1;
    public static final int PENDULUM_AND_RATCHET = 2;
    public static final int PENDULUM_AND_RATCHET_AND_SPRING = 3;
    public static final int ROTATING_CLOCK = 4;
    public static final int ROTATING_CLOCK_WITH_ARROW_1 = 5;
    public static final int ROTATING_CLOCK_WITH_ARROW_2 = 6;
    public static final int ROTATING_CLOCK_WITH_ARROW_3 = 7;
    public static final int ROTATING_CLOCK_WITH_ARROW_4 = 8;

    Clock output1;
    /// Total pendulums score
    public double output2;
    /// Clock type
    public int output3;
    /// Pendulums
    public int[] output4_0;
    /// Pendulum gear sizes
    public int[] output4_1;
    /// Pendulum periods
    public double[] output4_2;
}
