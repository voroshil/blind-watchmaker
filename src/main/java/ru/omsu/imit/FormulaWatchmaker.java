package ru.omsu.imit;

public class FormulaWatchmaker implements IWatchmaker {
	static int[] T = {1, 60, 60*60, 24*60*60, 7 * 24 * 60 * 60, 365 * 24 * 60 * 60};
	static double L = 1e6;

	double calcft(Clock c, int t, int[] HP, double[] PP){
		int i;
		double minf = L * t;
		boolean found = false;

		for(i=0; i<Clock.OBJECT_COUNT; i++){
			if (HP[i] == 0)
				continue;
			found = true;
			double f = Math.abs(t - PP[i]);
			if (f < minf)
				minf = f;
		}
		if (!found)
			return 0;

		if (minf * L < t)
			return L;
		else
			return ((double)t) / minf;
	}
	double calcFp(Clock c, int[] HP, double[] PP){
		double sum = 0;
		int i;
		double minp = L;
		for(i=0; i<Clock.OBJECT_COUNT; i++) {
			if (HP[i] == 1 && minp > PP[i])
				minp = PP[i];
		}
		for(i=0; i<T.length; i++){
			if (minp < T[i])
				sum += calcft(c, T[i], HP, PP);
		}
		return sum;
	}

	double calcFs(Clock c, int[]GH, double[] PH){
		double sum = 0;
		int i;
		for(i=0; i<T.length; i++){
			sum += calcft(c, T[i], GH, PH);
		}
		return sum;
	}
	void calcHPPP(Clock c, int[] HP, double[] PP){
		int i,j;
		int sum;
		for(i=0; i<Clock.OBJECT_COUNT; i++){
			HP[i] = 0;
			PP[i] = 0.0;
		}
		for(i=Clock.MIN_HAND; i<=Clock.MAX_HAND; i++){
			HP[i] = 1;
			if (c.conn[i][Clock.BASE] == Clock.NO_CONNECTION) {
				HP[i] = 0;
				continue;
			}
			sum = 0;
			for(j=Clock.MIN_HAND; j<=Clock.MAX_HAND; j++) {
				sum += (c.conn[i][j] != Clock.NO_CONNECTION) ? 1 : 0;
			}
			if (sum != 0) {
				HP[i] = 0;
				continue;
			}
			sum = 0;
			for(j=Clock.MIN_GEAR; j<=Clock.MAX_GEAR; j++) {
				sum += (c.conn[i][j] != Clock.NO_CONNECTION) ? 1 : 0;
			}
			if (sum != 1) {
				HP[i] = 0;
				continue;
			}
			if (c.sizes[i] < 1000){
				HP[i] = 0;
				continue;
			}
			PP[i] = 2.007 * Math.sqrt(((double)c.sizes[i]) / 10000);
		}
	}

	void calcGB(Clock c, int[] GB){
		int i,j;
		int size;
		int sum;

		size = 0;
		for(i=Clock.MIN_GEAR; i<=Clock.MAX_GEAR; i++){
			GB[i] = (c.conn[i][Clock.BASE] != Clock.NO_CONNECTION) ? 1 : 0;
			size += GB[i];
		}

		sum = 0;
		for(i=Clock.MIN_GEAR; i<=Clock.MAX_GEAR; i++){
			if (GB[i] == 0)
				continue;
			// Исключаем симметричные соединения.
			for(j=i+1; j<=Clock.MAX_GEAR; j++){
				sum += (c.conn[i][j] != Clock.NO_CONNECTION) ? 1 : 0;
			}
		}
		if (sum  >= size){
			// Если условие нарушается, то множество - пустое
			for(i=Clock.MIN_GEAR; i<= Clock.MAX_GEAR; i++){
				GB[i] = 0;
			}
		}
	}
	int calcGPK(Clock c, int[]HP, int[]GB, int[][]GPK){
		int i,j,h;
		int k=0;
		int sum;
		boolean found;

		// Расчет GP(0)
		for(i=Clock.MIN_GEAR; i<= Clock.MAX_GEAR; i++){
			GPK[0][i] = 0;
			if (GB[i] == 0)
				continue;
			if (c.conn[i][Clock.RATCHET] != Clock.TEETH_CONNECTION)
				continue;
			if (c.conn[i][Clock.BASE] != Clock.AXIL_CONNECTION)
				continue;
			found = false;
			for(h=Clock.MIN_HAND; h<=Clock.MAX_HAND; h++){
				if (HP[h] == 0)
					continue;
				if (c.conn[Clock.RATCHET][h] != Clock.AXIL_CONNECTION)
					continue;

				found = true;
				break;
			}
			if (found){
				GPK[0][i] = 1;
			}
		}
		// Расчет GP(k+1)
		for(k=1; k<Clock.MAX_GEAR; k++) {
			found = false;
			for (i = Clock.MIN_GEAR; i <= Clock.MAX_GEAR; i++) {
				GPK[k][i] = GPK[k-1][i];
				if (GB[i] == 0)
					continue;
				if (GPK[k-1][i] == 1)
					continue;
				if (c.conn[i][Clock.BASE] != Clock.AXIL_CONNECTION)
					continue;
				sum = 0;
				for(j=Clock.MIN_GEAR; j<=Clock.MAX_GEAR; j++){
					if (GPK[k-1][j] == 1 && c.conn[i][j] != Clock.NO_CONNECTION) // j \in GP(k-1) and c'_ij=1
						sum++;
				}
				if (sum == 1){
					GPK[k][i] = 1;
					found = true;
				}
			}
			if (!found){
				return k-1;
			}
		}
		return k;
	}

	void calcPGK(Clock c, int maxK, double[]PP, int[][]GPK, double[][]PGK){
		int i,j,k;

		k = 0;
		for(i=Clock.MIN_GEAR; i<=Clock.MAX_GEAR; i++){
			PGK[k][i] = 0.0;
			if (GPK[k][i] == 1){
				for(j=Clock.MIN_HAND; j<=Clock.MAX_HAND;j++) {
					if (c.conn[i][Clock.RATCHET] != Clock.NO_CONNECTION && c.conn[j][Clock.RATCHET] != Clock.NO_CONNECTION)
						PGK[k][i] = Math.round(1000 * PP[j] * (c.sizes[i] / 10000)) / 1000;
				}
			}
		}
		for(k=1; k<=maxK; k++){
			for(i=Clock.MIN_GEAR; i<=Clock.MAX_GEAR; i++){
				PGK[k][i] = PGK[k-1][i];
				if (GPK[k][i] == 0) // i \in GP(k)
					continue;
				for(j=Clock.MIN_GEAR; j<=Clock.MAX_GEAR; j++){
					if (GPK[k-1][j] == 0) // j \in GP(k-1)
						continue;
					if (c.conn[i][j] == Clock.AXIL_CONNECTION){
						PGK[k][i] = PGK[k-1][j];
					}else if (c.conn[i][j] == Clock.TEETH_CONNECTION){
						PGK[k][i] = Math.round(1000*(-PGK[k-1][j] * (c.sizes[j]/10000)) / (c.sizes[i]/10000))/1000;
					}
				}
			}
		}
	}
	void calcPGK_f(Clock c, int maxK, double[]PP, int[][]GPK, double[][]PGK){
		int i,j,k;
		long[][]PGK_m = new long[Clock.OBJECT_COUNT][Clock.OBJECT_COUNT];
		long[][]PGK_d = new long[Clock.OBJECT_COUNT][Clock.OBJECT_COUNT];
		k = 0;
		for(i=Clock.MIN_GEAR; i<=Clock.MAX_GEAR; i++){
			PGK_m[k][i] = 0;
			PGK_d[k][i] = 1;
			if (GPK[k][i] == 1){
				for(j=Clock.MIN_HAND; j<=Clock.MAX_HAND;j++) {
					if (c.conn[i][Clock.RATCHET] != Clock.NO_CONNECTION && c.conn[j][Clock.RATCHET] != Clock.NO_CONNECTION)
						PGK_m[k][i] = (int)(PP[j] * c.sizes[i]);
					    PGK_d[k][i] = 10000;
					    long nod = Clock.nod(PGK_m[k][i], PGK_d[k][i]);
						PGK_m[k][i] /= nod;
					    PGK_d[k][i] /= nod;
				}
			}
		}
		for(k=1; k<=maxK; k++){
			for(i=Clock.MIN_GEAR; i<=Clock.MAX_GEAR; i++){
				PGK_m[k][i] = PGK_m[k-1][i];
				PGK_d[k][i] = PGK_d[k-1][i];
				if (GPK[k][i] == 0) // i \in GP(k)
					continue;
				for(j=Clock.MIN_GEAR; j<=Clock.MAX_GEAR; j++){
					if (GPK[k-1][j] == 0) // j \in GP(k-1)
						continue;
					if (c.conn[i][j] == Clock.AXIL_CONNECTION){
						PGK_m[k][i] = PGK_m[k-1][j];
						PGK_d[k][i] = PGK_d[k-1][j];
					}else if (c.conn[i][j] == Clock.TEETH_CONNECTION){
						PGK_m[k][i] = -PGK_m[k-1][j] * c.sizes[j] ;
						PGK_d[k][i] = PGK_d[k-1][j] * c.sizes[i] ;
						long nod = Clock.nod(PGK_m[k][i], PGK_d[k][i]);
						PGK_m[k][i] /= nod;
						PGK_d[k][i] /= nod;
					}
				}
			}
		}
		for(k=0; k<=maxK; k++){
			for(i=Clock.MIN_GEAR; i<=Clock.MAX_GEAR; i++){
				PGK[k][i] = ((double)PGK_m[k][i])/PGK_d[k][i];
			}
		}
	}
	boolean calcGHPH(Clock c, int[]GP, double[]PG, int[]GH, double[]PH){
		int i,j;
		int sum0;
		int sum1;
		boolean found = false;
		for(i=0; i<Clock.OBJECT_COUNT; i++) {
			GH[i] = 0;
			PH[i] = 0.0;
			if (i<Clock.MIN_GEAR || i > Clock.MAX_GEAR)
			  continue;
			if (GP[i] == 0)
				continue;
			if (c.conn[i][Clock.SPRING] == Clock.NO_CONNECTION)
					continue;
			found = true;
		}
//		if (c.conn[Clock.SPRING][Clock.BASE] == Clock.NO_CONNECTION)
//			return false;
		if (!found)
			return false;
		for(i=0; i<Clock.OBJECT_COUNT; i++){
			GH[i] = 0;
			PH[i] = 0.0;
			if (i < Clock.MIN_HAND || i > Clock.MAX_HAND)
				continue;

			sum0 = 0;
			sum1 = 0;
			for(j=Clock.MIN_GEAR; j<=Clock.MAX_GEAR;j++) {
				if (c.conn[i][j] != Clock.NO_CONNECTION) {
					if (GP[j] == 1) {
						PH[i] = PG[j];
						sum1++;
					} else {
						sum0++;
					}
				}
			}
			if (sum1 == 1 && sum0 == 0){
				GH[i] = 1;
			}else{
				PH[i] = 0.0;
			}
		}
		return true;
	}
	private void checkGearSize(Clock c) {
		for(int i=Clock.MIN_GEAR; i<=Clock.MAX_GEAR; i++) {
			if (c.sizes[i] < 40000)
				c.sizes[i] = 40000;
		}
	}
	public void checkBaseTransient(Clock c){
		int i,j;
		boolean found = true;
		while(found) {
			found = false;
			for (i = Clock.MIN_GEAR; i <= Clock.MAX_GEAR; i++) {
				if (c.conn[i][Clock.BASE] != Clock.NO_CONNECTION && c.conn[Clock.BASE][i] != Clock.NO_CONNECTION)
					continue;
				c.conn[i][Clock.BASE] = Clock.AXIL_CONNECTION;
				c.conn[Clock.BASE][i] = Clock.AXIL_CONNECTION;
				for (j = Clock.MIN_GEAR; j< Clock.MAX_GEAR; j++){
					if (c.conn[j][Clock.BASE] != Clock.NO_CONNECTION && c.conn[j][Clock.BASE] != Clock.NO_CONNECTION)
						continue;
					if (c.conn[i][j] != Clock.AXIL_CONNECTION)
						continue;
					c.conn[j][Clock.BASE] = Clock.AXIL_CONNECTION;
					c.conn[Clock.BASE][j] = Clock.AXIL_CONNECTION;
					found = true;
				}
			}
		}
	}

	int calcUniqieHands(double[]PH){
		int[]hands = new int[T.length];
		int i;
		int t;
		int result = 0;
		for(i=0; i<hands.length;i++){
			hands[i] = -1;
		}
		for(t=0; t<T.length;t++){
			double min = 0;
			hands[t] = -1;
			for(i=Clock.MIN_HAND; i<=Clock.MAX_HAND;i++) {
				if (hands[t] < 0 || min > Math.abs(T[t]-PH[i])) {
					hands[t] = i;
					min = Math.abs(T[t] - PH[i]);
				}
			}
		}
		result = 0;
		for(t=0;t<T.length;t++){
			for(i=t+1; i<T.length;i++){
				if (hands[t] == hands[i]){
					hands[i] = -1;
				}else if (hands[t] < hands[i]){
					int tmp = hands[t];
					hands[t] = hands[i];
					hands[i] = tmp;
				}
			}
			if (hands[t] != -1){
				result++;
			}
		}
		return result;
	}

	public ClockState clocktest(Clock clock2) {
		Clock clock = Clock.cloneTested(clock2);
		checkGearSize(clock);
		checkBaseTransient(clock);
		int[] HP = new int[Clock.OBJECT_COUNT];
		double[] PP = new double[Clock.OBJECT_COUNT];
		int[] GB = new int[Clock.OBJECT_COUNT];
		int[][] GPK = new int[Clock.OBJECT_COUNT][Clock.OBJECT_COUNT];
		double[][] PGK = new double[Clock.OBJECT_COUNT][Clock.OBJECT_COUNT];
		int[] GH = new int[Clock.OBJECT_COUNT];
		double[] PH = new double[Clock.OBJECT_COUNT];
		int maxK;

		calcHPPP(clock, HP, PP);
		calcGB(clock, GB);
		maxK = calcGPK(clock, HP, GB, GPK);
		//calcPGK(clock, maxK, PP, GPK, PGK);
		calcPGK_f(clock, maxK, PP, GPK, PGK);
		boolean hasSpring = calcGHPH(clock, GPK[maxK], PGK[maxK], GH, PH);

		double fp = calcFp(clock, HP, PP);
		double fs = calcFs(clock, GH, PH);
		int unique = calcUniqieHands(PH);
		ClockState state = new ClockState();
		state.output1 = clock2;
		state.output3 = ClockState.NO_PENDULUM;
		if (fp > 0)
			state.output3 = ClockState.ONLY_PENDULUM;
		if (checkRatchet(clock, HP)) {
			state.output3 = ClockState.PENDULUM_AND_RATCHET;
			fp += 1.0;
		}
		if (hasSpring){
			state.output3 = ClockState.PENDULUM_AND_RATCHET_AND_SPRING;
			fp += 1.0;
		}
		int hps = 0;
		int i;
		for(i=0; i<HP.length; i++){
			hps += HP[i];
		}
		if (fs > 0)
			state.output3 = ClockState.ROTATING_CLOCK + unique;

		state.output2 = fp + L * fs;

		state.output5_0 = new int[Clock.MAX_HAND-Clock.MIN_HAND+1];
		state.output5_1 = new int[Clock.MAX_HAND-Clock.MIN_HAND+1];
		state.output5_2 = new double[Clock.MAX_HAND-Clock.MIN_HAND+1];
		int p = 0;
		for(i=Clock.MIN_HAND; i<=Clock.MAX_HAND; i++){
			if (GH[i] != 0){
				state.output5_0[p] = i;
				state.output5_2[p] = PH[i];
				p++;
			}
		}
		for(;p<state.output5_0.length; p++){
			state.output5_0[p] = 0;
			state.output5_2[p] = 0.0;
		}
		return state;
	}

	private boolean checkRatchet(Clock c, int[] HP) {
		int i;
		for(i=0; i<Clock.OBJECT_COUNT;i++){
			 if (HP[i] == 0)
			 	continue;
			 if (c.conn[Clock.RATCHET][i] == Clock.AXIL_CONNECTION)
			 	return true;
		}
		return false;
	}
}
