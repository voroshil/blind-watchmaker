package ru.omsu.imit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Application{
    static Logger logger = LoggerFactory.getLogger(Application.class);

    public static void doSimulation(String filename, Generation pop1, IWatchmaker watchmaker, int iterations) throws IOException {
        for(int i=0; i<iterations; i++) {
            int[] types = pop1.nextGeneration(watchmaker);
            double score = 0.0;
            double avgScore = 0.0;
            ClockState state = pop1.clocks[0];
            for(int j=0; j<Generation.populationSize; j++){
                avgScore += score;
                if (pop1.clocks[j].output2 > score) {
                    score = pop1.clocks[j].output2;
                    state = pop1.clocks[j];
                }
            }
            avgScore /= Generation.populationSize;
//
//            System.out.println("Generation: " + pop1.getGenerationNumber());
//            System.out.println("pendulum: " + types[0]);
//            System.out.println("ratchet, spring gear not conn: " + types[1]);
//            System.out.println("ratchet, spring gear connected: " + types[2]);
//            System.out.println("proto-clock: " + types[3]);
//            System.out.println("one unique hand: " + types[4]);
//            System.out.println("two unique hands: " + types[5]);
//            System.out.println("three unique hands: " + types[6]);
//            System.out.println("four unique hands: " + types[7]);
//            System.out.println("9:: " + types[8]);
//            System.out.println("10:: " + types[9]);
//            System.out.println("");
            if (score < 1000000) {
                logger.info(String.format("[%05d] : %12.2f %12.2f %05d %05d %05d %05d %05d %05d %05d %05d %05d %05d",
                        pop1.getGenerationNumber(),
                        avgScore,
                        score,
                        types[0],
                        types[1],
                        types[2],
                        types[3],
                        types[4],
                        types[5],
                        types[6],
                        types[7],
                        types[8],
                        types[9]));
            }else{
                logger.info(String.format("[%05d] : %12.2fKK %12.2fKK %05d %05d %05d %05d %05d %05d %05d %05d %05d %05d",
                        pop1.getGenerationNumber(),
                        avgScore / 1000000,
                        score / 1000000,
                        types[0],
                        types[1],
                        types[2],
                        types[3],
                        types[4],
                        types[5],
                        types[6],
                        types[7],
                        types[8],
                        types[9]));
            }
            if (score > 1e6) {
//                for (int j = 0; j < state.output5_0.length; j++) {
//                    logger.debug(String.format("[%d %d %12.2f] ", state.output5_0[j], state.output5_1[j], state.output5_2[j]));
//                }
            }
//            if(score > 1e6) {
//                state.output1.prettyPrint(false);
//            }
//            System.out.println("");
//            state.output1.dump();
//            int[] gearsize = state.output1.gearSize();
//            double[]res = state.output1.calcRotations(state.output1.conn, gearsize, 1, 60);
//            System.out.println("RES="+res.length);
            if (pop1.getGenerationNumber() % 10 == 0)
                Generation.saveToFile(pop1, filename);
        }
    }
    public static void dumpTop10(Generation pop1, IWatchmaker watchmaker){
        double score = 0.0;
        for(int j=0; j<Generation.populationSize; j++){
            if (pop1.clocks[j].output2 > score) {
                score = pop1.clocks[j].output2;
            }
        }
        System.out.println("display clock with score="+score);
        int i=0;
        for(int j=0; j<Generation.populationSize && i < 10; j++){
            if (pop1.clocks[j].output2 == score) {
                System.out.println("Clock #"+j);
                Clock c = Clock.cloneTested(pop1.clocks[j].output1);
                //c.dump();
                c.prettyPrint(false);
                ClockState state = watchmaker.clocktest(c);
                for(int p=0; p<state.output5_0.length; p++) {
                    System.out.println("H"+state.output5_0[p]+":"+state.output5_2[p]);
                }
                i++;
            }
        }
    }
    public static void main(String args[]) throws IOException, ClassNotFoundException {

        //IWatchmaker watchmaker = new OriginalWatchmaker();
        IWatchmaker watchmaker = new FormulaWatchmaker();
        Generation.populationSize = 10000;
        //Generation pop1 = Generation.generateNew(watchmaker);
        Generation pop1 = Generation.loadFromFile("generations_00000000.dat");
        doSimulation("generations_%08d.dat", pop1, watchmaker, 2000);
        //dumpTop10(pop1, watchmaker);
    }
}