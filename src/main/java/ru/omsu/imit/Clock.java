package ru.omsu.imit;

import java.io.Serializable;
import java.util.Random;
import java.util.Stack;

public class Clock implements Serializable {
    public static final int NO_CONNECTION = 0;
    public static final int TEETH_CONNECTION = 1;
    public static final int AXIL_CONNECTION = 2;
    static final int MIN_GEAR = 0;
    static final int MAX_GEAR = 29;
    static final int MIN_HAND = 30;
    static final int MAX_HAND = 36;
    static final int RATCHET = 37;
    static final int SPRING = 38;
    static final int BASE = 39;
    static final int OBJECT_COUNT = 40;
    /*
     * 30 gears: 0:29
     * 7 hands: 30:36
     * 1 ratchet: 37
     * 1 spring: 38
     * 1 base: 39
     */
    int conn[][] = new int[40][40];
    int sizes[] = new int[40];
    static Random random = new Random();
    public Clock(Clock mother, Clock father){
        for(int i=0; i<40; i++) {
            for(int j=0; j<40; j++) {
                conn[i][j] = random.nextInt(2) == 0 ?
                        mother.conn[i][j] :
                        father.conn[i][j];
            }
            sizes[i] = random.nextInt(2) == 0 ? mother.sizes[i] : father.sizes[i];
        }
    }
    public Clock() {
        for(int i=0; i<OBJECT_COUNT; i++){
            for(int j=0; j<OBJECT_COUNT; j++){
                double rand = random.nextDouble();
                if (rand < 0.06){
                    conn[i][j] = TEETH_CONNECTION;
                }else if (rand < 0.1){
                    conn[i][j] = AXIL_CONNECTION;
                }else{
                    conn[i][j] = NO_CONNECTION;
                }
            }
            sizes[i] = random.nextInt(1000000);
        }
    }
    public void dump(){
        for(int i=0; i<40; i++){
            for(int j=0; j<40;j++){
                System.out.print(conn[i][j]);
            }
            System.out.println("");
        }
        System.out.println("");
        for(int i=0; i<40; i++){
            System.out.println(sizes[i]);
        }
        for(int i=MIN_GEAR; i<=MAX_GEAR; i++){
            for(int j=MIN_GEAR; j<=MAX_GEAR; j++){
                if (conn[i][j] != NO_CONNECTION){
                    System.out.println("G"+i+" <-> G"+j+" = "+conn[i][j]);
                }
            }
        }
    }
    /**
     *   Connectivity symmetry check.
     *   Connectivity is bidirectional, make sure the matrix is so.
     *   This is just a physical fact of the universe.  If A touches B, B must
     *   touch A.
     */
    void ensureSymmetry(){
        for (int r=0; r<BASE; r++) {
            for(int c = 0; c<BASE; c++) {
                if (conn[r][c] != conn[c][r]) {
                    int temp = random.nextInt(2);
                    if (temp == 1)
                        conn[r][c] = conn[c][r];
                    else
                        conn[c][r] = conn[r][c];
                }
            }
            // Don't connect to itself
            conn[r][r] = NO_CONNECTION;
        }
    }
    void shunting(int[] a){
        for(int i=0; i<a.length * 2; i++){
            int a1 = random.nextInt(a.length);
            int a2 = random.nextInt(a.length);
            if (a1 != a2){
                int temp = a[a1];
                a[a1] = a[a2];
                a[a2] = temp;
            }
        }
    }
    int[] findConnected(int r, int count){
        int[] indx = new int[count];
        int found = 0;
        for(int i=0; i<count; i++){
            if (conn[r][i] != Clock.NO_CONNECTION){
                indx[found++] = i;
            }
        }
        if (found == 0)
            return new int[0];

        int[] result = new int[found];
        for(int i=0; i<found; i++){
            result[i] = indx[i];
        }
        return result;
    }
    int[] findByConnectionType(int r, int connectionType, int count){
        int[] indx = new int[count];
        int found = 0;
        for(int i=0; i<count; i++){
            if (conn[r][i] == connectionType){
                indx[found++] = i;
            }
        }
        if (found == 0)
            return new int[0];

        int[] result = new int[found];
        for(int i=0; i<found; i++){
            result[i] = indx[i];
        }
        return result;
    }

    /**
     * Gears cannot be connected to more gears than teeth they have. Again, just
     * making the simulation physically realistic.
     */
    void checkGearConnectionToRealTeeth(){
        int[] gearsize = gearSize();
        for(int r=MIN_GEAR; r<=MAX_GEAR; r++) {
            int[] g = findByConnectionType(r, TEETH_CONNECTION, 30);
            if (g.length > gearsize[r]) {
                shunting(g);
                for(int temp=gearsize[r]; temp<g.length; temp++) {
                    conn[r][g[temp]] = NO_CONNECTION;
                    conn[g[temp]][r] = NO_CONNECTION;
                }
            }
        }
    }

    /**
     * Hands can only be connected to max 4 objects because they only have 4
     * connection points.
     */
    void checkHandConnections(){
        for (int r=MIN_HAND; r<=MAX_HAND; r++) {
            int[] g = findConnected(r, OBJECT_COUNT);
            if (g.length > 4) {
                shunting(g);
                for(int temp=4; temp<g.length; temp++) {
                    conn[r][g[temp]] = NO_CONNECTION;
                    conn[g[temp]][r] = NO_CONNECTION;
                }
            }
        }
    }
    /**
     * Ratchet can only be connected to 1 object via its teeth and 2 objects
     * via its center, again because of the number of connection points.
     */
    void checkRatchetConnections(){
        int[] g = findByConnectionType(RATCHET, AXIL_CONNECTION, OBJECT_COUNT);
        if (g.length > 1) {
            shunting(g);
            for(int temp=1; temp<g.length; temp++) {
                conn[RATCHET][g[temp]] = NO_CONNECTION;
                conn[g[temp]][RATCHET] = NO_CONNECTION;
            }
        }
        g = findByConnectionType(RATCHET, TEETH_CONNECTION, OBJECT_COUNT);
        if (g.length > 1) {
            shunting(g);
            for(int temp=1; temp<g.length; temp++) {
                conn[RATCHET][g[temp]] = NO_CONNECTION;
                conn[g[temp]][RATCHET] = NO_CONNECTION;
            }
        }
    }

    /**
     * Spring can only be attached to max 4 objects.  Spring has 2 ends, each
     * end can connect to two objects (1 on each face of the spring.
     */
    void checkSpringConnection(){
        int[] g = findConnected(SPRING, OBJECT_COUNT);
        if (g.length > 4) {
            shunting(g);
            for(int temp=4; temp<g.length; temp++) {
                conn[SPRING][g[temp]] = NO_CONNECTION;
                conn[g[temp]][SPRING] = NO_CONNECTION;
            }
        }
    }
    int[] gearSize(){
        int gearsize[] = new int[MAX_GEAR-MIN_GEAR+1];
        for(int i=0; i<=MAX_GEAR-MIN_GEAR; i++){
            gearsize[i] = sizes[i] / 10000;
            if (gearsize[i] < 4)
                gearsize[i] = 4;
        }
        return gearsize;
    }
    public void selfTest(){
        ensureSymmetry();
        checkGearConnectionToRealTeeth();
        checkHandConnections();
        checkRatchetConnections();
        checkSpringConnection();
        for (int r=0; r<OBJECT_COUNT; r++) {
            for (int c = 0; c<r; c++) {
                conn[c][r] = conn[r][c];
            }
        }
    }
    public int[][] distanceTo(int[] primarynodes){
        return distanceTo(conn, primarynodes);
    }
    /**
     * The circuit_distance.m function finds the shortest path between every
     % pair of gears.

     * @param primarynodes
     * @return
     */
    public static int[][] distanceTo(int[][]c, int[] primarynodes){
        //Integer[] dummy = new Integer[0];

        int d[][] = new int[30][30];
        for(int i=0; i<30; i++){
            for(int j=0;j<30; j++){
                d[i][j] = 1;
            }
        }
        int[] dtemp = new int[30];
        for(int i=0;i<30;i++){
            dtemp[i] = 1000000;
        }
        //Integer[][][]pathmat = new Integer[30][][];
        //%xxx = waitbar(0,'Searching for Paths');

        //%postmat{1:length(c)} = [];
        int [][]postmat = new int[30][];
        for (int i = 0; i<30; i++) {
            int nonZero = 0;
            for(int j=0; j<30; j++){
                if (c[i][j] != NO_CONNECTION){
                    nonZero++;
                }
            }
            postmat[i] = new int[nonZero];
            nonZero = 0;
            for(int j=0; j<30; j++){
                if (c[i][j] != NO_CONNECTION){
                    postmat[i][nonZero++] = j;
                }
            }
        }
        Stack<Integer> s = new Stack<Integer>();
        for (int N = 0; N<30; N++){
            boolean found = false;
            for(int i=0; i<primarynodes.length; i++){
                if (primarynodes[i] == N){
                    found = true;
                    break;
                }
            }
            if(!found)
                continue;

            //%if rem(N,5) == 0
            //%    waitbar(N/length(c));
            //%end
            s.clear();
            s.push(N);
            int pathlength = 1;

            while (pathlength > 0) {
                int nn = s.peek();
                int[] post = postmat[nn];

                int foundtip = 0;
                int test = 0;
                for (test = 0; test<post.length; test++){
                    if (dtemp[post[test]] > pathlength) {
                        dtemp[post[test]] = pathlength;
                        s.push(test);
                        //pathmat[N][post[test]] = s.toArray(dummy);
                        pathlength = pathlength + 1;
                        foundtip = 1;
                        break;
                    }
                }

                if (test==0 && foundtip == 0) {
                    s.pop();
                    pathlength = pathlength - 1;
                } else if (test == post.length && foundtip == 0) {
                    s.pop();
                    pathlength = pathlength - 1;
                }
            }

            for(int i=0; i<30; i++) d[N][i] = dtemp[i];
            dtemp = new int[30];
            for(int i=0;i<30;i++){
                dtemp[i] = 1000000;
            }

        }

        for(int i=0; i<30; i++){
            for(int j=0; j<30; j++){
                if (d[i][j] == 1000000){
                    d[i][j] = -1;
                }
            }
        }

        //close(xxx);
        return d;
        //return {d, pathmat};
    }

    /**
     *
     */
    public int[][] checkBase(){
        int[][] gconn2 = new int[30][30];
        for(int i=0;i<30; i++) {
            for(int j=0;j<30;j++) {
                gconn2[i][j] = conn[i][j] == AXIL_CONNECTION ? AXIL_CONNECTION : NO_CONNECTION;
            }
        }

        int[] keep = new int[30];
        for(int i=0; i<keep.length; i++){
            keep[i] = 0;
        }
        int[] baseg = findConnected(Clock.BASE, 30);
        for(int i=0;i<baseg.length;i++) {
            keep[baseg[i]] = 1;
        }

    //   The circuit_distance.m function finds the shortest path between every
    //   pair of gears.
        int[][] d2 = Clock.distanceTo(gconn2, baseg);

        for (int g=0; g<baseg.length; g++) {
            for(int j=0; j<30; j++){
                if (d2[baseg[g]][j] >= 0){
                    keep[j] = 1;
                }
            }
        }
        for(int i=0; i<30; i++){
            if (keep[i] == 1) {
                conn[Clock.BASE][i] = 1;
            }
        }

        int[][] gconn = new int[30][30];
        for(int i=0; i<30; i++){
            for(int j=0;j<30; j++){
                gconn[i][j] = conn[i][j];
            }
        }

        for (int r=0; r<30; r++) {
            if (keep[r] == 0) {
                for(int i=0;i<30; i++) {
                    if (gconn[r][i] != Clock.NO_CONNECTION)
                        gconn[r][i] = Clock.NO_CONNECTION;
                    if (gconn[i][r] != Clock.NO_CONNECTION)
                        gconn[i][r] = Clock.NO_CONNECTION;
                }
            }
        }
        return gconn;
    }
    /**
     *
     */
    public static Clock cloneTested(Clock ck) {
        Clock clock = cloneUntested(ck);
        clock.selfTest();
        return clock;
    }

    public static Clock cloneUntested(Clock ck) {
        Clock clock = new Clock();
        copyConnection(clock, ck);
        for(int i=0;i<40; i++){
            clock.sizes[i] = ck.sizes[i];
        }
        return clock;
    }

    public static void copyConnection(Clock temp99, Clock conn) {
        for(int i=0;i<40; i++) {
            for (int j = 0; j < 40; j++) {
                temp99.conn[i][j] = conn.conn[i][j];
            }
        }
    }
    public void mutate(double one, double two) {
        for (int m=0; m<2; m++){

            int mutloc = random.nextInt(1640);
            double muts = random.nextDouble();
            int mut;

            if (muts <= one) {
                mut = Clock.TEETH_CONNECTION;
            } else if (muts <= one + two) {
                mut = Clock.AXIL_CONNECTION;
            } else {
                mut = Clock.NO_CONNECTION;
            }
            if (mutloc >= 1600) {
                sizes[mutloc-1600] = random.nextInt(1000000);
            } else {
                conn[mutloc / 40][mutloc % 40] = mut;
            }
        }
    }

    public void prettyPrint(boolean halt) {
        //int[][]gconn = checkBase();
        int[][]gconn = conn;
        Stack<Integer> s = new Stack<Integer>();
        int ratchetGear = -1;
        for(int i=0; i<30; i++){
            if (conn[RATCHET][i] != NO_CONNECTION || conn[i][RATCHET] != NO_CONNECTION) {
                ratchetGear = i;
                break;
            }
        }
        if (ratchetGear == -1) {
            for(int i=0; i<30; i++){
                System.out.print(conn[RATCHET][i]);
            }
            System.out.println("");
            throw new RuntimeException("Ratchet gear not found");
        }
        int ratchetHand = -1;
        for(int i=MIN_HAND; i<=MAX_HAND; i++) {
            if (conn[i][RATCHET] != NO_CONNECTION){
                ratchetHand = i;
                break;
            }
        }
        if (ratchetHand == -1)
            throw new RuntimeException("Ratchet hand not found");

        double pendulumPeriod = 2.007 * Math.sqrt(((double)sizes[ratchetHand]) / 10000);
        double period = Math.round(1000 * pendulumPeriod * (sizes[ratchetGear] / 10000)) / 1000;
        int m1_base;
        int d1_base;
        if (period < 0.001){
            throw new RuntimeException("Pendulum period " + period + " too small");
        }else if (period > 1000) {
            throw new RuntimeException("Pendulum period " + period + " too large");
        }else if (period <= 1.0){
            d1_base = 1000;
            m1_base = (int)(1000 * period);
        }else if (period <= 10.0){
            d1_base = 100;
            m1_base = (int)(100 * period);
        }else if (period <= 100.0){
            d1_base = 10;
            m1_base = (int)(10 * period);
        }else{
            d1_base = 1;
            m1_base = (int)(period);
        }
        long nod_m1_d1_base = nod(m1_base, d1_base);
        m1_base /= nod_m1_d1_base;
        d1_base /= nod_m1_d1_base;

        System.out.println("Pendulum period = " + period + "("+m1_base+"/"+d1_base+")");
        for(int i=MIN_HAND; i<=MAX_HAND; i++){
            int handGear = -1;
            for(int j=0;j<30; j++){
                if (conn[i][j] != NO_CONNECTION){
                    handGear = j;
                    break;
                }
            }
            if (handGear == -1)
                continue;
            //System.out.println("HG:"+i+" "+handGear);
            int[] path = deikstra(gconn, ratchetGear, handGear);
            if (path.length == 0)
                continue;
            System.out.print("R -> G"+ratchetGear+" ");
            long m1 = m1_base;
            long d1 = d1_base;
            for(int j=1; j<path.length; j++){
                if (gconn[path[j-1]][path[j]] == AXIL_CONNECTION) {
                    System.out.print(" = G" + path[j] + " ");
                }else if (gconn[path[j-1]][path[j]] == TEETH_CONNECTION) {
                    int s1 = sizes[path[j-1]]/10000;
                    int s2 = sizes[path[j]]/10000;
                    if (s1<4) s1=4;
                    if (s2<4) s2=4;
                    m1 *= s1;
                    d1 *= s2;

                    System.out.print(" <-("+s1+"/"+s2+")-> G" + path[j]);
                }
            }
            System.out.print(" => H"+i+" ("+m1+"/"+d1+") ");
            long nod_m1_d1 = nod(m1, d1);
            m1 /= nod_m1_d1;
            d1 /= nod_m1_d1;
            System.out.println("("+m1+"/"+d1+") ");
        }
//        System.out.println("R <-> G"+ratchetGear);
//        s.push(ratchetGear);
//        int seen = 0;
//        while (!s.empty()){
//            boolean found =false;
//            for(int i=seen; i<s.size() ; i++){
//                int nn = s.get(i);
//                for(int j=0; j<30; j++){
//                    if (nn != j && !s.contains(j)){
//                        boolean transitive = false;
//                        for (int k=0; k<=MAX_HAND; k++){
//                            if (k!= nn && k !=j && conn[j][k]!=NO_CONNECTION) {
//                                transitive = true;
//                                break;
//                            }
//                        }
//                        if (!transitive)
//                            continue;
//                        if (conn[nn][j] == TEETH_CONNECTION){
//                            int s1 = sizes[nn]/10000;
//                            int s2 = sizes[j]/10000;
//                            if (s1 < 4) s1 = 4;
//                            if (s2 < 4) s2 = 4;
//                            System.out.print(String.format("G%d -> G%d %d/%d ", nn, j, s1, s2));
//                            s.push(j);
//                            found = true;
//                            for (int k = MIN_HAND; k <= MAX_HAND; k++) {
//                                if (conn[k][j] != NO_CONNECTION) {
//                                    System.out.print("[H" + k + "] ");
//                                }
//                            }
//                            System.out.println("");
//                        }else if (conn[nn][j] == AXIL_CONNECTION){
//                            System.out.print(String.format("G%d = G%d ", nn, j));
//                            s.push(j);
//                            found = true;
//                            for (int k = MIN_HAND; k <= MAX_HAND; k++) {
//                                if (conn[k][j] != NO_CONNECTION) {
//                                    System.out.print("[H" + k + "] ");
//                                }
//                            }
//                            System.out.println("");
//                        }
//                    }
//                }
//            }
//            seen++;
//            if (found) break;
//        }
//        for(int i=0; i<40; i++) {
//            for (int j = 0; j < 40; j++) {
//                String name1;
//                String name2;
//
//                if (i>=MIN_GEAR && i<=MAX_GEAR)
//                    name1="GEAR #"+i;
//                else if (i>=MIN_HAND && i<=MAX_HAND)
//                    name1="HAND #"+i;
//                else if (i==RATCHET)
//                    name1="RATCHET";
//                else if (i==SPRING)
//                    name1="SPRING";
//                else if(i==BASE)
//                    name1="BASE";
//                else
//                    name1="??? #"+i;
//
//                if (j>=MIN_GEAR && j<=MAX_GEAR)
//                    name2="GEAR #"+j;
//                else if (j>=MIN_HAND && j<=MAX_HAND)
//                    name2="HAND #"+j;
//                else if (j==RATCHET)
//                    name2="RATCHET";
//                else if (j==SPRING)
//                    name2="SPRING";
//                else if(j==BASE)
//                    name2="BASE";
//                else
//                    name2="??? #"+j;
//
//                if (conn[i][j] == TEETH_CONNECTION){
//                        System.out.println(String.format("%s <- %s -> %s", name1, "TEETH", name2));
//                }else if(conn[i][j] == AXIL_CONNECTION)
//                    System.out.println(String.format("%s <- %s -> %s", name1, "AXIL", name2));
//            }
//        }
//        for(int i=0; i<40; i++){
//            System.out.println("GEAR #"+i+" = "+ (sizes[i]/10000));
//        }
        if (halt) throw new RuntimeException();
    }
    public static long nod(long n1, long n2){
        if (n1 == 0)
            return n2;
        if (n2 == 0)
            return n1;
        if (n1 == n2)
            return n1;
        if (n1 == 1)
            return 1;
        if (n2 == 1)
            return 1;

        if (n2 > n1)
            return nod (n2, n1);

        while(true){
            long t = n1 % n2;
            if (t == 0) break;
            n1 = n2;
            n2 = t;
        }
        return n2;
    }

    private int[] deikstra(int[][]conn, int ratchetGear, int handGear) {
        int gears = MAX_GEAR - MIN_GEAR +1;
        int[] weights = new int[gears];
        int[] path = new int[gears];
        for(int i=0; i<gears; i++){
            weights[i] = -1; // inf
        }
        weights[ratchetGear-MIN_GEAR] = 0;
        int first = 0;
        int last = 0;
        path[first] = ratchetGear;

        boolean changed = true;
        while(changed && last<gears) {
            changed = false;
            for (int k = first; k <= last; k++) {
                int current = path[k] - MIN_GEAR;
                for (int i = 0; i < gears; i++) {
                    if (current == i)
                        continue;
                    if (conn[current + MIN_GEAR][i + MIN_GEAR] == NO_CONNECTION)
                        continue;
                    if (weights[i] == -1) { // inf
                        weights[i] = weights[current] + 1;
                        changed = true;
                        path[++last] = i + MIN_GEAR;
                    } else if (weights[i] > weights[current] + 1) {
                        weights[i] = weights[current] + 1;
                        changed = true;
                    }
                }
            }
        }
        int weight = weights[handGear-MIN_GEAR];
        if (weight == -1)
            return new int[0];
        int[] result = new int[weight+1];
        result[weight] = handGear;
        for(int i=weight-1; i>=0;i--){
            int current = result[i+1];
            for(int j=0; j<gears; j++){
                if (conn[j+MIN_GEAR][current] != NO_CONNECTION && weights[j] == i){
                    result[i] = j + MIN_GEAR;
                    break;
                }
            }
        }
        return result;
    }
}
