package ru.omsu.imit;

import org.junit.*;

import static org.junit.Assert.assertEquals;

public class ClockTest {
    Clock createZeroClock(){
        Clock c = new Clock();
        for(int i=0; i<40; i++){
            for(int j=0; j<40; j++) {
                c.conn[i][j] = 0;
            }
            c.sizes[i] = 0;
        }
        return c;
    }
    @Test
    public void test1(){
        Clock c = createZeroClock();

        c.conn[0][Clock.MIN_HAND] = Clock.AXIL_CONNECTION;
        OriginalWatchmaker maker = new OriginalWatchmaker();
        ClockState state = maker.clocktest(c);

        assertEquals(ClockState.NO_PENDULUM, state.output3);
    }
    @Test
    public void test2(){
        Clock c = createZeroClock();

        c.conn[Clock.MIN_HAND][0] = Clock.AXIL_CONNECTION;
        c.conn[Clock.MIN_HAND][Clock.BASE] = Clock.AXIL_CONNECTION;
        c.conn[0][Clock.MIN_HAND] = Clock.AXIL_CONNECTION;
        c.conn[Clock.BASE][Clock.MIN_HAND] = Clock.AXIL_CONNECTION;
        c.sizes[Clock.MIN_HAND] = 20000;

        OriginalWatchmaker maker = new OriginalWatchmaker();
        ClockState state = maker.clocktest(c);

        assertEquals(ClockState.ONLY_PENDULUM, state.output3);
    }

    @Test
    public void test3(){
        Clock c = createZeroClock();

        c.conn[Clock.MIN_HAND][0] = Clock.AXIL_CONNECTION;
        c.conn[Clock.MIN_HAND][Clock.BASE] = Clock.AXIL_CONNECTION;
        c.conn[0][Clock.MIN_HAND] = Clock.AXIL_CONNECTION;
        c.conn[Clock.BASE][Clock.MIN_HAND] = Clock.AXIL_CONNECTION;
        c.sizes[Clock.MIN_HAND] = 20000;

        c.conn[Clock.RATCHET][Clock.MIN_HAND] = Clock.TEETH_CONNECTION;
        c.conn[Clock.MIN_HAND][Clock.RATCHET] = Clock.TEETH_CONNECTION;

        c.conn[Clock.RATCHET][1] = Clock.AXIL_CONNECTION;
        c.conn[1][Clock.RATCHET] = Clock.AXIL_CONNECTION;

        c.conn[1][Clock.BASE] = Clock.AXIL_CONNECTION;
        c.conn[Clock.BASE][1] = Clock.AXIL_CONNECTION;

        c.conn[1][Clock.SPRING] = Clock.AXIL_CONNECTION;
        c.conn[Clock.SPRING][1] = Clock.AXIL_CONNECTION;

        OriginalWatchmaker maker = new OriginalWatchmaker();
        ClockState state = maker.clocktest(c);

        assertEquals(ClockState.ROTATING_CLOCK_WITH_ARROW_1, state.output3);
    }
    @Test
    public void test4(){
        String map =
                "0000002000022000000000000000000000000000\n" +
                "0000000000000000000000002000000000000000\n" +
                "0010000200000000100000000000000000000000\n" +
                "0000000000000012000000000000000000000000\n" +
                "0000000000000000000000000000000000000000\n" +
                "0000020202000000000000000000010000000000\n" +
                "2000000000000000000100000000000000000000\n" +
                "0020020200000020000000100000100000000000\n" +
                "0000000000000000000000000000000000020000\n" +
                "0000000000000000000002001100000010000000\n" +
                "0000000000000000000000000000002000000000\n" +
                "2000000000000000000000000000100000000000\n" +
                "2000000200000000000002000020000000000000\n" +
                "0000000000000000000000200000000000000000\n" +
                "0001100000000000000000000000100000000000\n" +
                "0000000000000000020000000000000000001000\n" +
                "0010000000000000000000000000000000000000\n" +
                "0000000100000000000000000100000000000000\n" +
                "0000000000000000000020000000100010002002\n" +
                "0000001000000000000000002000000000000000\n" +
                "0000000000000200002000000000100000000000\n" +
                "0000000200110000000000000100000000000000\n" +
                "0000000100000000000100010000000000002000\n" +
                "0000000000000000000000100000200000000000\n" +
                "0000000001000000000200000000000000000212\n" +
                "0000000001000000000001000000000000000000\n" +
                "0000000000000000000000000010000000000000\n" +
                "0100000000000000000000200000000000000000\n" +
                "0000000100000010000000000000000000000000\n" +
                "0000010000000000000000000000000000000000\n" +
                "0000000000000000000000000000020000000000\n" +
                "0000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000\n" +
                "0000000000000000000000000000000000000000\n" +
                "0000000000001000000000000000000000000002\n" +
                "0000000020000000000000000000000000000101\n" +
                "0000000000000001000000000000000000000000\n" +
                "0000000000000000000000002000000000010000\n" +
                "0000000000000000000000001000000000000000\n" +
                "1000000000000200002000002001000010020000\n";
        Clock instance = new Clock();
        instance.sizes[0]=128157;
        instance.sizes[1]=865742;
        instance.sizes[2]=584336;
        instance.sizes[3]=788513;
        instance.sizes[4]=911753;
        instance.sizes[5]=379220;
        instance.sizes[6]=833693;
        instance.sizes[7]=560368;
        instance.sizes[8]=639338;
        instance.sizes[9]=186824;
        instance.sizes[10]=355866;
        instance.sizes[11]=170221;
        instance.sizes[12]=416012;
        instance.sizes[13]=535042;
        instance.sizes[14]=388303;
        instance.sizes[15]=246238;
        instance.sizes[16]=761241;
        instance.sizes[17]=765719;
        instance.sizes[18]=388613;
        instance.sizes[19]=990253;
        instance.sizes[20]=686329;
        instance.sizes[21]=95745;
        instance.sizes[22]=327567;
        instance.sizes[23]=334047;
        instance.sizes[24]=8185;
        instance.sizes[25]=162328;
        instance.sizes[26]=36061;
        instance.sizes[27]=361719;
        instance.sizes[28]=395184;
        instance.sizes[29]=667276;
        instance.sizes[30]=809240;
        instance.sizes[31]=563018;
        instance.sizes[32]=986731;
        instance.sizes[33]=978516;
        instance.sizes[34]=204121;
        instance.sizes[35]=990962;
        instance.sizes[36]=255915;
        instance.sizes[37]=884132;
        instance.sizes[38]=994671;
        instance.sizes[39]=544325;

        String[] lines = map.split("\n");
        for(int i=0; i<40; i++){
            for(int j=0; j<40; j++) {
                char c = lines[i].charAt(j);
                if (c == '2') {
                    instance.conn[i][j] = 2;
                }else if (c=='1') {
                    instance.conn[i][j] = 1;
                }else if (c == '0') {
                    instance.conn[i][j] = 0;
                }else{
                    instance.conn[i][j]=3;
                }
            }
        }
    //instance.dump();
        OriginalWatchmaker maker = new OriginalWatchmaker();
        maker.clocktest(instance);
    }
    @Test
    public void test5(){
        String map=
                "2010200000000000000000000001000000000002\n" +
                        "0000000000200000200000000000010000000000\n" +
                        "1000000100000000000000000000200010000001\n" +
                        "0000000000000000000000000000000000000000\n" +
                        "2000000000001000000000000000001000000000\n" +
                        "0000000000000100000000000000000000000000\n" +
                        "0000000010000000000000000000000000000000\n" +
                        "0010000100000010000000000000000000000000\n" +
                        "0000001000200000000000000000000000000000\n" +
                        "0000000100000200000000000000100000002002\n" +
                        "0001000020000000000000000000200000000000\n" +
                        "0000000000000000000000000000000000000000\n" +
                        "0000100000000002000000000000000000000000\n" +
                        "0010000000100000000000020000000000000000\n" +
                        "0000000000000000000000000000000000000000\n" +
                        "0000000000002000100000000000000000000001\n" +
                        "0200000000000001000000000000000000200212\n" +
                        "0000000000000000000002000000000000000000\n" +
                        "0000000000000100000000000000200000000000\n" +
                        "0000000000000000000000000000000000000000\n" +
                        "0000000000000000000000000000000002000000\n" +
                        "0000000002000100000000000000010000000000\n" +
                        "0000000000000000000000001000000000000000\n" +
                        "0000000020000200000000200000000000000000\n" +
                        "0000000000000000000000100000000000000000\n" +
                        "0000000000000000000000000000000000000000\n" +
                        "0000000000000000000000000000200000000000\n" +
                        "1000000000000000000000000002000000020000\n" +
                        "0020000001000000000000000020000000000000\n" +
                        "0000000000000000000000000000000000000000\n" +
                        "0000100000000000000000000000000000000000\n" +
                        "0000000000000000000000000000000000000000\n" +
                        "0010000000000000000000000000000000000000\n" +
                        "0000000000000000000020000000000000000101\n" +
                        "0000000000000000200000000000000000000000\n" +
                        "0000000000000000000000000002000000000000\n" +
                        "0000000002000000000000000000000000000000\n" +
                        "0000000000000000200000000000000001000000\n" +
                        "0000000000000000200000000000000000000000\n" +
                        "2010100002000001200000000000000001000000\n";
        String s = "16248\n" +
                "791493\n" +
                "969826\n" +
                "274861\n" +
                "803508\n" +
                "92186\n" +
                "444618\n" +
                "125319\n" +
                "242510\n" +
                "292611\n" +
                "766425\n" +
                "741781\n" +
                "333528\n" +
                "329408\n" +
                "630639\n" +
                "998501\n" +
                "34200\n" +
                "195245\n" +
                "522262\n" +
                "640760\n" +
                "80532\n" +
                "31823\n" +
                "705534\n" +
                "518720\n" +
                "626576\n" +
                "665852\n" +
                "883310\n" +
                "997306\n" +
                "44755\n" +
                "358309\n" +
                "328468\n" +
                "166428\n" +
                "227912\n" +
                "579213\n" +
                "211145\n" +
                "250083\n" +
                "186886\n" +
                "774918\n" +
                "913537\n" +
                "676399\n";
        Clock instance = new Clock();
        String[] lines = map.split("\n");
        String[] slines = s.split("\n");
        for(int i=0; i<40; i++){
            for(int j=0; j<40; j++) {
                char c = lines[i].charAt(j);
                if (c == '2') {
                    instance.conn[i][j] = 2;
                }else if (c=='1') {
                    instance.conn[i][j] = 1;
                }else if (c == '0') {
                    instance.conn[i][j] = 0;
                }else{
                    instance.conn[i][j]=3;
                }
            }
            instance.sizes[i] = Integer.parseInt(slines[i]);
        }
        //instance.dump();
        OriginalWatchmaker maker = new OriginalWatchmaker();
        maker.clocktest(instance);
   }

}